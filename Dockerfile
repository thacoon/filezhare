FROM    rust:slim as builder

WORKDIR /app

RUN     apt-get update \
        && apt-get upgrade -y \
        && apt-get clean \
        && apt-get autoremove \
        && rm -rf /var/lib/apt/lists/*

COPY    . .

RUN     cargo build --release

# ------------------------------------------------

FROM    ubuntu:latest

EXPOSE  8000

WORKDIR /app

RUN     apt-get update \
        && apt-get upgrade -y \
        && apt-get clean \
        && apt-get autoremove \
        && rm -rf /var/lib/apt/lists/*

COPY    --from=builder /app/target/release/filezhare .
COPY    --from=builder /app/Rocket.toml .
COPY    --from=builder /app/media ./media

RUN     useradd -m -g users user \
        && chown user:users -R /app

USER    user

CMD     ["./filezhare"]
