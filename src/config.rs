use rocket::fairing::AdHoc;
use rocket::Build;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub temp_dir: String,
    pub media_root: String,
    pub api_key: String,
}

pub fn attach_config(rocket: rocket::Rocket<Build>) -> rocket::Rocket<Build> {
    let figment = rocket.figment();

    let _config: Config = figment.extract().expect("config");

    rocket.attach(AdHoc::config::<Config>())
}
