use rocket::http::Status;
use rocket::response::{NamedFile, Responder};
use rocket::{response, Request, Response};

pub struct ImageFile(pub Option<NamedFile>);

// Ref: https://api.rocket.rs/master/rocket/response/trait.Responder.html
// TODO Add support for Last-Modified header
#[rocket::async_trait]
impl<'r> Responder<'r, 'static> for ImageFile {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        if self.0.is_none() {
            Response::build().status(Status::NotModified).ok()
        } else {
            let image = self.0.unwrap();

            // Customizing HTTP headers
            // Ref: https://github.com/SergioBenitez/Rocket/issues/95#issuecomment-354824883
            Response::build_from(image.respond_to(req)?)
                .raw_header("Cache-Control", "max-age=604800") // 7 days TTL
                .ok()
        }
    }
}
