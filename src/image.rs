use crate::config;
use crate::guard::IfModifiedSinceHeader;
use crate::response::image_file::ImageFile;
use chrono::{DateTime, Local};
use image::imageops::FilterType;
use rocket::http::Status;
use rocket::response::NamedFile;
use rocket::State;
use std::fs;
use std::ops::Not;
use std::path::Path;

#[derive(Debug, PartialEq, FromForm)]
pub struct Size {
    width: u32,
    height: u32,
}

#[get("/image/<filename>?<size>")]
pub async fn get_image(
    filename: &str,
    size: Size,
    modified_since: IfModifiedSinceHeader,
    config: &State<config::Config>,
) -> Result<ImageFile, Status> {
    let media_path = Path::new(&config.media_root).join(filename);

    if was_modified_since(&media_path, modified_since).not() {
        return Ok(ImageFile(None));
    }

    // TODO If image was updated, the cache was not invalidated.
    //  For Now, it will return the resized version of the old image when available, which is wrong.
    let resized_filename = format!("w-{}-h-{}-{}", size.width, size.height, filename);
    let cached_path = Path::new(&config.temp_dir).join(&resized_filename);
    if cached_path.exists() {
        let file = NamedFile::open(Path::new(&config.temp_dir).join(&resized_filename));
        return match file.await {
            Ok(i) => return Ok(ImageFile(Some(i))),
            Err(_) => Err(Status::NotFound),
        };
    }

    let image;
    match image::open(&media_path) {
        Ok(i) => image = i,
        Err(_) => return Err(Status::NotFound),
    }

    // TODO Research about different interpolation filters
    let image = image.resize(size.width, size.height, FilterType::Lanczos3);

    let file;
    match image.save(Path::new(&config.temp_dir).join(&resized_filename)) {
        Ok(_) => file = NamedFile::open(Path::new(&config.temp_dir).join(&resized_filename)),
        Err(_) => return Err(Status::NotFound),
    }

    return match file.await {
        Ok(i) => Ok(ImageFile(Some(i))),
        Err(_) => Err(Status::NotFound),
    };
}

fn was_modified_since(path: &Path, if_modified_since: IfModifiedSinceHeader) -> bool {
    // Returns true on any error, eg. if header does not exists
    if if_modified_since.time.is_some() {
        let if_modified_since = if_modified_since.time.unwrap();

        let last_modified;
        match fs::metadata(path) {
            Ok(m) if m.modified().is_ok() => last_modified = m.modified().unwrap(),
            Ok(_) => return true,
            Err(_) => return true,
        }

        let last_modified: DateTime<Local> = DateTime::from(last_modified);

        last_modified.with_timezone(&if_modified_since.timezone()) > if_modified_since
    } else {
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use image::RgbImage;
    use rocket::http::{Header, Status};
    use rocket::local::blocking::Client;

    use crate::server::rocket;
    use chrono::Duration;
    use std::fs;
    use std::fs::File;

    fn create_image(path: &Path) {
        let imgbuf: RgbImage = image::ImageBuffer::new(256, 256);
        imgbuf.save(path).unwrap();
    }

    // TODO Mocks would be great as now I test the correct logic by comparing images

    // Note: Every test should use a different uuid for the files as we currently are not mocking
    // but creating/deleting real files and if they have the same uuid the tests may panic as
    // files are not immediately deleted which may results in "double free" of files and panic

    #[test]
    fn get_image_by_filename_returns_304_if_file_was_not_modified() {
        let image_path = "./media/4207f5f6-f163-4a68-8b8b-024fe66c22f0.png";
        create_image(Path::new(image_path));

        // At least one seconds needs to pass as otherwise this tests fails as it is executed too fast
        let time: DateTime<Local> = Local::now() + Duration::seconds(1);

        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");
        let if_modified_since = Header::new("If-Modified-Since", time.to_rfc2822());

        let response = client
            .get("/image/4207f5f6-f163-4a68-8b8b-024fe66c22f0.png?size.width=200&size.height=200")
            .header(accept)
            .header(if_modified_since)
            .dispatch();

        assert_eq!(response.status(), Status::NotModified);

        fs::remove_file(image_path).unwrap();
    }

    #[test]
    fn get_image_by_filename_returns_file_if_was_modified() {
        let image_path = "./media/f3adcd81-f849-4873-bf94-ad04f433dcc9.jpeg";
        create_image(Path::new(image_path));

        let time: DateTime<Local> = Local::now() - Duration::hours(1);

        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");
        let if_modified_since = Header::new("If-Modified-Since", time.to_rfc2822());

        let response = client
            .get("/image/f3adcd81-f849-4873-bf94-ad04f433dcc9.jpeg?size.width=96&size.height=96")
            .header(accept)
            .header(if_modified_since)
            .dispatch();

        let cache_image_path =
            Path::new("./media/tmp/w-96-h-96-f3adcd81-f849-4873-bf94-ad04f433dcc9.jpeg");

        let image = image::open(Path::new(&image_path)).unwrap();
        let image = image.resize(96, 96, FilterType::Lanczos3);
        let expected_image_path =
            Path::new("./media/tmp/expected-f3adcd81-f849-4873-bf94-ad04f433dcc9.jpeg");
        image.save(expected_image_path).unwrap();
        let expected_image = fs::read(expected_image_path).unwrap();

        assert_eq!(response.status(), Status::Ok);
        assert!(cache_image_path.exists());
        assert_eq!(response.into_bytes().unwrap(), expected_image);

        fs::remove_file(image_path).unwrap();
        fs::remove_file(cache_image_path).unwrap();
        fs::remove_file(expected_image_path).unwrap();
    }

    #[test]
    fn get_image_by_filename_resize_it_and_cache_it() {
        let image_path = "./media/5dbaf8d8-5a44-47f3-a49f-f78976b950d2.png";
        create_image(Path::new(image_path));

        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");

        let response = client
            .get("/image/5dbaf8d8-5a44-47f3-a49f-f78976b950d2.png?size.width=200&size.height=200")
            .header(accept)
            .dispatch();

        let cache_image_path =
            Path::new("./media/tmp/w-200-h-200-5dbaf8d8-5a44-47f3-a49f-f78976b950d2.png");

        let image = image::open(Path::new(&image_path)).unwrap();
        let image = image.resize(200, 200, FilterType::Lanczos3);
        let expected_image_path =
            Path::new("./media/tmp/expected-5dbaf8d8-5a44-47f3-a49f-f78976b950d2.png");
        image.save(expected_image_path).unwrap();
        let expected_image = fs::read(expected_image_path).unwrap();

        assert_eq!(response.status(), Status::Ok);
        assert!(cache_image_path.exists());
        assert_eq!(response.into_bytes().unwrap(), expected_image);

        fs::remove_file(image_path).unwrap();
        fs::remove_file(cache_image_path).unwrap();
        fs::remove_file(expected_image_path).unwrap();
    }

    #[test]
    fn get_image_return_cached_image_if_exists() {
        let cached_image_path = "./media/tmp/w-256-h-256-22980831-ca83-4407-af74-5473f0df5f1a.png";
        create_image(Path::new(cached_image_path));

        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");

        let response = client
            .get("/image/22980831-ca83-4407-af74-5473f0df5f1a.png?size.width=256&size.height=256")
            .header(accept)
            .dispatch();

        let expected_image = fs::read(cached_image_path).unwrap();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.into_bytes().unwrap(), expected_image);

        fs::remove_file(cached_image_path).unwrap();
    }

    #[test]
    fn get_image_returns_404_if_image_and_cache_version_does_not_exists() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");

        let response = client
            .get("/image/aaab2c96-3148-43ac-865f-d88de0b4285a.png?size.width=256&size.height=256")
            .header(accept)
            .dispatch();

        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn get_image_by_filename_adds_cache_ttl_header() {
        let image_path = "./media/4211f8d0-a2eb-4389-84e2-c0c3812fd174.png";
        create_image(Path::new(image_path));

        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");

        let response = client
            .get("/image/4211f8d0-a2eb-4389-84e2-c0c3812fd174.png?size.width=200&size.height=200")
            .header(accept)
            .dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.headers().get("Cache-Control").count(), 1);
        assert_eq!(
            response.headers().get_one("Cache-Control"),
            Some("max-age=604800")
        ); // 60 s * 60 min * 24 h * 7 d

        fs::remove_file(image_path).unwrap();
    }

    #[test]
    fn was_modified_since_return_true_if_modified() {
        let path = Path::new("./media/tmp/test.txt");
        let _file = File::create(&path).unwrap();

        let time = IfModifiedSinceHeader {
            time: Some(DateTime::from(Local::now()) - Duration::hours(1)),
        };

        let result = was_modified_since(path, time);

        assert!(result);

        fs::remove_file(&path).unwrap();
    }

    #[test]
    fn was_modified_since_return_false_if_not_modified() {
        let path = Path::new("./media/tmp/test.txt");
        let _file = File::create(&path).unwrap();

        let time = IfModifiedSinceHeader {
            time: Some(DateTime::from(Local::now())),
        };

        let result = was_modified_since(path, time);

        assert!(!result);

        fs::remove_file(&path).unwrap();
    }

    #[test]
    fn was_modified_since_return_false_if_time_is_none() {
        let path = Path::new("./media/tmp/test.txt");

        let time = IfModifiedSinceHeader { time: None };

        let result = was_modified_since(path, time);

        assert!(result);
    }
}
