#[macro_use]
extern crate rocket;

mod compression;
mod config;
mod errors;
mod file;
mod guard;
mod image;
mod response;
mod security;
mod server;

use server::rocket;

#[rocket::main]
async fn main() {
    rocket().launch().await.ok();
}
