use std::ops::Not;
use std::path::Path;

use rocket::data::TempFile;
use rocket::form::Form;
use rocket::http::ContentType;
use rocket::State;
use rocket_contrib::json::{json, JsonValue};
use rocket_contrib::uuid::Uuid;

use crate::errors::{
    error_as_json, FILE_COULD_NOT_BE_PERSISTED, FILE_TYPE_NOT_ALLOWED, INVALID_FILE_TYPE,
};
use crate::{config, security};
use rocket::response::NamedFile;

#[derive(FromForm)]
pub struct FileUploadForm<'f> {
    id: Uuid,
    #[field(validate = len(1..))]
    allowed_file_types: Vec<String>,
    file: TempFile<'f>, // https://rocket.rs/master/guide/requests/#temporary-files
}

#[get("/file/<filename>")]
pub async fn get_file(filename: &str, config: &State<config::Config>) -> Option<NamedFile> {
    NamedFile::open(Path::new(&config.media_root).join(filename))
        .await
        .ok()
}

#[post("/upload", data = "<form>")]
pub async fn upload(
    mut form: Form<FileUploadForm<'_>>,
    config: &State<config::Config>,
    _key: security::ApiKey,
) -> JsonValue {
    let mime_type;
    match extract_file_type(form.file.path()) {
        Some(m) => mime_type = m,
        None => return error_as_json(INVALID_FILE_TYPE),
    }

    if form
        .allowed_file_types
        .contains(&mime_type.to_string())
        .not()
    {
        return error_as_json(FILE_TYPE_NOT_ALLOWED);
    }

    let filename = format!("{}.{}", form.id.to_string(), mime_type.extension().unwrap());
    let path = Path::new(&config.media_root).join(filename.clone());

    // TODO May cause problems, see https://api.rocket.rs/master/rocket/data/enum.TempFile.html#hazards
    if form.file.persist_to(&path).await.is_err() {
        return error_as_json(FILE_COULD_NOT_BE_PERSISTED);
    }

    json!({"status": "ok", "resource": filename})
}

fn extract_file_type(path_option: Option<&Path>) -> Option<ContentType> {
    let path;
    match path_option {
        Some(p) => path = p,
        None => return None,
    }

    let result;
    match tree_magic_mini::from_filepath(path) {
        Some(r) => result = r,
        None => return None,
    }

    let mime_type;
    match ContentType::parse_flexible(result) {
        Some(m) => mime_type = m,
        None => return None,
    }

    Some(mime_type)
}

#[cfg(test)]
mod tests {
    extern crate image;

    use super::*;

    use self::image::RgbImage;
    use rocket::http::{Header, Status};
    use rocket::local::blocking::Client;
    use std::fs::File;
    use std::io::{Read, Write};
    use std::path::PathBuf;
    use std::str;
    use std::{env, fs, io};

    use crate::server::rocket;

    const BOUNDARY: &str = "--------------------------------XYZ";

    fn get_temp_path(file_name: &str) -> PathBuf {
        let temp_directory = env::temp_dir();
        temp_directory.join(file_name)
    }

    fn write_to_file(file_name: &str, buf: &[u8]) -> PathBuf {
        let temp_directory = env::temp_dir();
        let temp_file = temp_directory.join(file_name);

        let mut file = File::create(&temp_file).unwrap();

        file.write_all(buf).unwrap();

        temp_file
    }

    fn create_image(path: &Path) {
        let imgbuf: RgbImage = image::ImageBuffer::new(256, 256);
        imgbuf.save(path).unwrap();
    }

    fn create_tmp_image(file_name: &str) -> PathBuf {
        let path_buf = get_temp_path(file_name);
        create_image(path_buf.as_path());
        path_buf
    }

    fn image_data(
        boundary: &str,
        id: &str,
        allowed_file_type: &str,
        image_file_name: &str,
        image: &str,
    ) -> io::Result<Vec<u8>> {
        // TODO Multiple file types are allowed
        // https://stackoverflow.com/questions/51397872/how-to-post-an-image-using-multipart-form-data-with-hyper
        // https://golangbyexample.com/multipart-form-data-content-type-golang/
        let mut data = Vec::new();

        // id
        write!(data, "--{}\r\n", boundary)?;
        write!(data, "Content-Disposition: form-data; name=\"id\"\r\n")?;
        write!(data, "\r\n")?;

        write!(data, "{}", id)?;
        write!(data, "\r\n")?;

        // allowed_file_types
        write!(data, "--{}\r\n", boundary)?;
        write!(
            data,
            "Content-Disposition: form-data; name=\"allowed_file_types\"\r\n"
        )?;
        write!(data, "\r\n")?;

        write!(data, "{}", allowed_file_type)?;
        write!(data, "\r\n")?;

        // image data
        write!(data, "--{}\r\n", boundary)?;
        write!(
            data,
            "Content-Disposition: form-data; name=\"file\"; filename=\"{}\"\r\n",
            image_file_name
        )?;
        write!(data, "Content-Type: image/jpeg\r\n")?;
        write!(data, "\r\n")?;

        let path_buf = create_tmp_image(image);
        let mut f = File::open(path_buf.as_path())?;
        f.read_to_end(&mut data)?;
        write!(data, "\r\n")?;

        // end
        write!(data, "--{}--\r\n", boundary)?;

        Ok(data)
    }

    #[test]
    fn get_file_by_filename() {
        let image_path = "./media/6d85055c-b214-4f33-b173-59a51da8b785.png";
        create_image(Path::new(image_path));

        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");

        let response = client
            .get("/file/6d85055c-b214-4f33-b173-59a51da8b785.png")
            .header(accept)
            .dispatch();

        let expected_image = fs::read(image_path).unwrap();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.into_bytes().unwrap(), expected_image);

        fs::remove_file(image_path).unwrap();
    }

    #[test]
    fn get_file_return_404_if_not_exists() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let accept = Header::new("Accept", "application/json");

        let response = client
            .get("/file/6d85055c-b214-4f33-b173-59a51da8b785.jpeg")
            .header(accept)
            .dispatch();

        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn upload_requires_authorization() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let response = client.post("/upload").dispatch();

        assert_eq!(response.status(), Status::Unauthorized);
    }

    #[test]
    fn save_file_with_given_id_as_file_name() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let authorization = Header::new("Authorization", "secret-api-key");
        let accept = Header::new("Accept", "application/json");
        let content_type = Header::new(
            "Content-Type",
            format!("multipart/form-data; boundary={}", BOUNDARY),
        );

        let response = client
            .post("/upload")
            .header(content_type)
            .header(authorization)
            .header(accept)
            .body(
                image_data(
                    BOUNDARY,
                    "1588a509-3517-49f2-9dea-1791c2e99db9",
                    "image/jpeg",
                    "image.jpeg",
                    "image.jpeg",
                )
                .unwrap(),
            )
            .dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(
            response.into_string().unwrap(),
            "{\"resource\":\"1588a509-3517-49f2-9dea-1791c2e99db9.jpeg\",\"status\":\"ok\"}"
        );
        assert!(Path::new("./media/1588a509-3517-49f2-9dea-1791c2e99db9.jpeg").exists());

        fs::remove_file("./media/1588a509-3517-49f2-9dea-1791c2e99db9.jpeg").unwrap();
    }

    #[test]
    fn save_file_uses_the_guessed_file_extension_in_filename() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let authorization = Header::new("Authorization", "secret-api-key");
        let accept = Header::new("Accept", "application/json");
        let content_type = Header::new(
            "Content-Type",
            format!("multipart/form-data; boundary={}", BOUNDARY),
        );

        let response = client
            .post("/upload")
            .header(content_type)
            .header(authorization)
            .header(accept)
            .body(
                image_data(
                    BOUNDARY,
                    "1588a509-3517-49f2-9dea-1791c2e99db9",
                    "image/png",
                    "image.jpeg",
                    "image.png",
                )
                .unwrap(),
            )
            .dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(
            response.into_string().unwrap(),
            "{\"resource\":\"1588a509-3517-49f2-9dea-1791c2e99db9.png\",\"status\":\"ok\"}"
        );
        assert!(Path::new("./media/1588a509-3517-49f2-9dea-1791c2e99db9.png").exists());

        fs::remove_file("./media/1588a509-3517-49f2-9dea-1791c2e99db9.png").unwrap();
    }

    #[test]
    fn save_file_only_accepts_if_file_type_is_allowed() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let authorization = Header::new("Authorization", "secret-api-key");
        let accept = Header::new("Accept", "application/json");
        let content_type = Header::new(
            "Content-Type",
            format!("multipart/form-data; boundary={}", BOUNDARY),
        );

        let response = client
            .post("/upload")
            .header(content_type)
            .header(authorization)
            .header(accept)
            .body(
                image_data(
                    BOUNDARY,
                    "1588a509-3517-49f2-9dea-1791c2e99db9",
                    "image/png",
                    "image.jpeg",
                    "image.jpeg",
                )
                .unwrap(),
            )
            .dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(
            response.into_string().unwrap(),
            "{\"code\":101,\"message\":\"File type not allowed\",\"status\":\"error\"}"
        );
        assert!(!Path::new("./media/1588a509-3517-49f2-9dea-1791c2e99db9.png").exists());
    }

    #[test]
    fn extract_file_type_returns_none_if_param_is_none() {
        let result = extract_file_type(None);

        assert_eq!(result, None);
    }

    // TODO Mocking of 3-rd party crates (eg. tree_magic_mini) would be great

    #[test]
    fn extract_file_type_as_plain_text_content_type() {
        let data: [u8; 100] = [0; 100];
        let path = write_to_file("binary.jpg", &data);

        let result = extract_file_type(Some(path.as_path()));

        assert_eq!(result, Some(ContentType::Binary));
    }

    #[test]
    fn extract_file_type_as_jpeg_content_type() {
        let path_buf = create_tmp_image("image.jpeg");

        let result = extract_file_type(Some(path_buf.as_path()));

        assert_eq!(result, Some(ContentType::JPEG));
    }
}
