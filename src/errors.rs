use rocket_contrib::json::{json, JsonValue};

pub struct ErrorType {
    pub code: i32,
    pub message: &'static str,
}

pub fn error_as_json(error: ErrorType) -> JsonValue {
    // TODO Log errors, at least the server errors
    return json!({
        "status": "error",
        "message": error.message,
        "code": error.code,
    });
}

// Server errors
pub const FILE_COULD_NOT_BE_PERSISTED: ErrorType = ErrorType {
    code: 1,
    message: "File could not be persisted",
};

// Client errors
pub const INVALID_FILE_TYPE: ErrorType = ErrorType {
    code: 100,
    message: "Invalid file type",
};

pub const FILE_TYPE_NOT_ALLOWED: ErrorType = ErrorType {
    code: 101,
    message: "File type not allowed",
};
