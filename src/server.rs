use crate::compression::attach_compression;
use crate::config;
use crate::file::{get_file, upload};
use crate::image::get_image;
use rocket::Build;

pub fn rocket() -> rocket::Rocket<Build> {
    let rocket = rocket::build().mount("/", routes![get_file, get_image, upload]);
    let rocket = attach_compression(rocket);
    config::attach_config(rocket)
}
