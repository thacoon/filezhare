use flate2::read::GzEncoder;
use rocket::fairing::{self, Info};
use rocket::http::{ContentType, MediaType};
use rocket::{Build, Request, Response};
use std::io;
use std::io::{Read, Seek};

pub struct Gzip;

struct Context {
    exclusions: Vec<MediaType>,
}

impl Default for Context {
    fn default() -> Context {
        Context {
            exclusions: vec![
                MediaType::parse_flexible("application/gzip").unwrap(),
                MediaType::parse_flexible("application/zip").unwrap(),
                MediaType::parse_flexible("image/*").unwrap(),
                MediaType::parse_flexible("video/*").unwrap(),
                MediaType::parse_flexible("application/wasm").unwrap(),
                MediaType::parse_flexible("application/octet-stream").unwrap(),
            ],
        }
    }
}

#[rocket::async_trait]
impl fairing::Fairing for Gzip {
    fn info(&self) -> Info {
        fairing::Info {
            name: "Gzip compression",
            kind: fairing::Kind::Response,
        }
    }

    // There exists a compression feature in rocket_contrib but it is not enabled currently and needs some work
    // See https://github.com/SergioBenitez/Rocket/blob/master/contrib/lib/src/lib.rs#L52
    // I have used the following resources to implement this
    // https://github.com/SergioBenitez/Rocket/issues/195
    // https://github.com/SergioBenitez/Rocket/tree/master/contrib/lib/src/compression
    // https://api.rocket.rs/master/rocket/fairing/trait.Fairing.html
    // https://github.com/Ameobea/Rocket/commit/eab53cf979cd12aed3d8cc805bf49f0e1e31d2dc
    // Note: I implemented this with pretty much trial&error as I have no clue what I am doing
    // Note: Actually if you use traefik as a reverse proxy you could use it to do the compressing
    // See: https://doc.traefik.io/traefik/middlewares/compress/
    // TODO The compression is not really async, fix it
    async fn on_response<'r>(&self, req: &'r Request<'_>, res: &mut Response<'r>) {
        // Dont compress all content types, eg. exlude images
        // See: https://webmasters.stackexchange.com/questions/8382/is-gzipping-images-worth-it-for-a-small-size-reduction-but-overhead-compressing
        if is_excluded(res.content_type()) {
            return;
        }

        let headers = req.headers();
        if headers
            .get("Accept-Encoding")
            .any(|e| e.to_lowercase().contains("gzip"))
        {
            let body = async {
                let body = res.body_mut().to_bytes().await.unwrap_or_default();
                let mut compressor =
                    GzEncoder::new(body.as_slice(), flate2::Compression::default());

                let mut buf = Vec::with_capacity(body.len());
                compressor
                    .read_to_end(&mut buf)
                    .expect("Attempt to read to end, but failed");

                buf
            };

            let mut data = std::io::Cursor::new(body.await);
            let size = data
                .seek(io::SeekFrom::End(0))
                .expect("Attempted to retrieve size by seeking, but failed.");
            data.seek(io::SeekFrom::Start(0))
                .expect("Attempted to reset body by seeking after getting size.");

            res.set_raw_header("Content-Encoding", "gzip");
            res.set_sized_body(Some(size as usize), data);
        }
    }
}

fn is_excluded(content_type: Option<ContentType>) -> bool {
    return match content_type {
        Some(content_type) => {
            let ctx = Context::default();
            ctx.exclusions.iter().any(|exc_media_type| {
                if exc_media_type.sub() == "*" {
                    *exc_media_type.top() == *content_type.top()
                } else {
                    *exc_media_type == *content_type.media_type()
                }
            })
        }
        None => true,
    };
}

pub fn attach_compression(rocket: rocket::Rocket<Build>) -> rocket::Rocket<Build> {
    rocket.attach(Gzip)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn specific_content_types_are_excluded() {
        // test a few
        assert!(is_excluded(Some(ContentType::JPEG)));
        assert!(is_excluded(Some(ContentType::PNG)));
        assert!(is_excluded(Some(ContentType::GZIP)));
        assert!(is_excluded(Some(ContentType::WASM)));
    }

    #[test]
    fn other_content_types_are_not_excluded() {
        // test a few
        assert!(!is_excluded(Some(ContentType::HTML)));
        assert!(!is_excluded(Some(ContentType::JavaScript)));
        assert!(!is_excluded(Some(ContentType::PDF)));
    }

    #[test]
    fn none_content_type_is_excluded() {
        assert!(is_excluded(None));
    }
}
