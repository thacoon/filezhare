use chrono::{DateTime, FixedOffset};
use rocket::request::{FromRequest, Outcome, Request};

pub struct IfModifiedSinceHeader {
    pub time: Option<DateTime<FixedOffset>>,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for IfModifiedSinceHeader {
    // Always returns success as If-Modified-Since header should be optional
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, ()> {
        let header = req.headers().get_one("If-Modified-Since").unwrap_or("");
        let time = parse_if_modified_since_header(header);
        return Outcome::Success(IfModifiedSinceHeader { time });
    }
}

fn parse_if_modified_since_header(header: &str) -> Option<DateTime<FixedOffset>> {
    // Ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-Modified-Since
    match DateTime::parse_from_rfc2822(header) {
        // If time is in future it fails parsing and returns an error
        Ok(d) => Some(d),
        Err(_) => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{Datelike, Timelike, Weekday};

    #[test]
    fn test_parse_correct_time_format() {
        let time = parse_if_modified_since_header("Wed, 21 Oct 2015 07:28:00 GMT");
        let time = time.unwrap();

        assert_eq!(time.weekday(), Weekday::Wed);
        assert_eq!(time.day(), 21);
        assert_eq!(time.month(), 10);
        assert_eq!(time.year(), 2015);
        assert_eq!(time.hour(), 7);
        assert_eq!(time.minute(), 28);
        assert_eq!(time.second(), 00);
    }

    #[test]
    fn test_parse_return_none_on_incorrect_time_format() {
        let time = parse_if_modified_since_header("this-is-no-date");

        assert_eq!(time, None);
    }
}
