Filezhare
=========

[![pipeline status](https://gitlab.com/thacoon/filezhare/badges/master/pipeline.svg)](https://gitlab.com/thacoon/filezhare/-/commits/master)

Filezhare is a simple, minimalistic and (hopefully) reliable service to upload and serve files.

## Goals

* Simple, it should be easy to deploy, maintain and use.
* Minimalistic, there should be no overengineering and the implementation is focused on the is state and not on features that may be added in the future.
* Reliable, if the service starts it should work as long as power is on. However, this is a long term goal.

## Getting Started

Filezhare is intended to run behind a reverse proxy, like [traefik](https://traefik.io/traefik/). Therefore, it does not support TLS as it is expected that the reverse proxy takes care of this.

The added `docker-compose.yml` serves as a reference to help you get started, you can run it with

```bash
$ docker-compose -f docker-compose.yml up
```

After starting you can for example upload an image with

```bash
$ curl -X POST \
    -H "Authorization: secret-api-key" \
    -H "Accept: application/json" \
    -F file=@/path/to/image.jpeg \
    -F id="c42b9739-9372-4b7e-8938-45325084e825" \
    -F allowed_file_types="image/jpeg" \
    -F allowed_file_types="image/png" \
    http://localhost:8000/upload
```

And then retrieve a resized version of it with

```bash
curl -X GET \
    -H "Accept: application/json" \
    -H "If-Modified-Since: Mon, 20 Apr 2020 04:20:00 GMT" \
    "http://localhost:8000/image/c42b9739-9372-4b7e-8938-45325084e825.jpeg?size.width=256&size.height=256" \
    >> ~/Downloads/resized.jpeg
```

Or retrieved a gziped version of some file. Gzipped is disabled for some content types like images as these are already compressed.

```bash
curl -X GET \
    --compressed \
    -H "Accept-Encoding: gzip" \
    "http://localhost:8000/file/c42b9739-9372-4b7e-8938-45325084e825.pdf" \
    >> ~/Downloads/compressed.pdf
```

## Configuration

Files that are uploaded must not exceed a maximum size. The maximal size of a file is set in the `Rocket.toml` to be 5 megabytes.

You can adjusts this maximum size by setting `ROCKET_LIMITS_FILE` and `ROCKET_LIMITS_DATA_FORM`, like so ROCKET_LIMITS="{file=8MiB, data-form=8MiB}"`.
This will assure that the maximum size of uploaded files does not exceed 8 megabytes.

However, the max. file size will be a bit less as files are sent inside a multipart forms with some additional data like allowed file types and an id.
As `ROCKET_LIMITS_DATA_FORM` defines the limit of multipart forms and `ROCKET_DATA_FILE` defines the limit for temporary files.
Therefore, the smaller one will also define the maximum size that can be uploaded.

## API Documentation

For always getting json response you need to set the `ACCEPT` header to `application/json`. If not set a html response on e.g. 404 errors may occur.

### Open Endpoints

Open endpoints require no Authentication.

* [Show file](/docs/file/get.md) : `GET /file/<filename>`
* [Show image](/docs/image/get.md) : `GET /image/<imagename>`

### Endpoints that require Authentication

Request are authenticated via an API token that needs to be set in the `Authentication` header on every request to one of the following endpoints.

* [Create file](/docs/upload/post.md) : `POST /upload`

## Development

### Run

Run the server locally which reload on source changes with the following command.

``bash
$ cargo run
```

Or if you want to watch for source code changes run

```bash
$ cargo watch -x run -w ./src
```

### Updating dependencies

Update dependencies as recorded in the lock file with `cargo update`.

### Developer tools

Usage of developers tools is advised. And is also checked on the CI.

### Code Style

Code needs to follow [community code style](https://github.com/rust-lang/rustfmt).

```bash
# Rustfmt needs to be added with
$ rustup component add rustfmt
# Automatically fix code style with
$ cargo fmt
```

### Compiler Warnings

Code should produce no compiler warnings.

```bash
# Automatically fix warnings with
$ cargo fix
```

### Linters

Code should not have any common mistakes than can be catched with [clippy](https://github.com/rust-lang/rust-clippy).

```bash
# Clippy needs to be installed with
$ rustup component add clippy
# Run Clippy on a Cargo project with
$ cargo clippy --all-targets --all-features --
```

## Disclaimer

I am a Rust noob, I just started again, learning the language and this is my first project with Rust + Rocket.
