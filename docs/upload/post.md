# Create file

Allow an authenticated service to upload a file.

**URL** : `/upload`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data constraints**

```json
{
    "id": "[uuid]",
    "allowed_file_types": "[min. 1, array of content types]"
    "file": "[blob data]",
}
```

List of supported content types can be found [here](https://api.rocket.rs/master/rocket/http/struct.ContentType.html#example-6).

**Data examples**

```json
{
    "id": "54d8e0da-5cd7-425e-8b15-3bc79e2b1de6",
    "allowed_file_types": ["image/png", "image/jpeg"],
    "file": blob
}
```

## Success Responses

**Condition** : Content type is supported.

**Code** : `200 OK`

**Content example** :

```json
{
    "status": "ok",
    "resource": "1588a509-3517-49f2-9dea-1791c2e99db9.jpeg",
}
```

The resource value defines the filename needed to retrieve this file later on.

## Error Response

**Condition** :

**Code** : `200 OK`

**Content example** :

```json
{
    "status": "error",
    "message": "human readable error message",
    "code": "01, 100 or 101"
}
```

More information about error codes can be found in [errors.rs](/src/errors.rs).

## Examples

### Python

Upload an image using **Python** and requests.

```python
import json
import mimetypes
import os
import uuid

import requests

fname = 'my_image.jpeg'
with open(fname, 'rb') as f:
    payload = (
        ('id', str(uuid.uuid4())),
        ('allowed_file_types', 'image/png'),
        ('allowed_file_types', 'image/jpeg'),
        ('file', (
            file.name,
            file.open(mode='rb'),
            mimetypes.MimeTypes().guess_type(file.name)[0],
        )),
    )

    r = requests.post(
        'http://127.0.0.1:8000/upload',
        headers={'Authorization': 'secret-api-key', 'Accept': 'application/json'},
        files=payload,
    )

    print(r.status_code)
    print(r.json())
```

### Go

Upload an image using **Go**.

```go
type FieldParam struct {
	Fieldname string
	Value     string
}

// resp, err := uploadFile(fileContent)

func uploadFile(fileContent []byte) (*http.Response, error) {
	request, err := newFileUploadRequest("http://localhost:8000/upload", fileContent)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Authorization", "my-secret-key")
	request.Header.Set("Accept", "application/json")

	client := &http.Client{}
	resp, err := client.Do(request)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

func newFileUploadRequest(uri string, fileContent []byte) (*http.Request, error) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	part, err := writer.CreateFormFile("file", "filename") // filename does not matter for filezhare
	if err != nil {
		return nil, err
	}

	if _, err = part.Write(fileContent); err != nil {
		return nil, err
	}

	extraParams := []FieldParam{
		FieldParam{
			Fieldname: "id",
			Value:     uuid.New().String(),
		},
		FieldParam{
			Fieldname: "allowed_file_types",
			Value:     "image/jpeg",
		},
		FieldParam{
			Fieldname: "allowed_file_types",
			Value:     "image/png",
		},
	}

	for _, param := range extraParams {
		if err = writer.WriteField(param.Fieldname, param.Value); err != nil {
			return nil, err
		}
	}

	err = writer.Close()
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest("POST", uri, body)
	if err != nil {
		return nil, err
	}

	request.Header.Add("Content-Type", writer.FormDataContentType())

	return request, nil
}
```

## Notes

* The endpoint guesses the content type of the upload file and checks if it one of the allowed file types. If not it rejects the request.
* The endpoint overwrites an existing file if it has the same id and content type as the new one.
