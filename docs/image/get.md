# Show Image

Download a specific image and offers functionality to retrieve a resized version of the original image.

**URL** : `/image/<imagename>?size.width=<width>&size.height=<height>`

**Method** : `GET`

**Auth required** : No

## Success Response

**Condition** : A valid image with the given imagename exists.

**Code** : `200 OK`

**Content examples**

A stream of the resized image.

## Error Response

**Condition** : The image does not exists or is not a valid image.

**Code** : `404 NOT FOUND`

## Notes

* Respects the `If-Modified-Since` header.
