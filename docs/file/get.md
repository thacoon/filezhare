# Show File

Download a specific file.

**URL** : `/file/<filename>`

**Method** : `GET`

**Auth required** : No

## Success Response

**Condition** : A file with the given filename exists.

**Code** : `200 OK`

**Content examples**

A stream of the file.

## Error Response

**Condition** : The file does not exists.

**Code** : `404 NOT FOUND`

## Notes

* Currently does not respects the `If-Modified-Since` header.
